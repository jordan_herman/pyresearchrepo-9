# Basic Functions
from collections import OrderedDict
import requests
import json
import pandas as pd


import pyresearch.parqschema
import pyresearch.hostaxis
import pyresearch.pdiddy
import pyresearch.util
import pyresearch.inventory
import pyresearch.cloud

docs_dict = OrderedDict()
docs_dict['parqschema'] = "Parquet functions for handling Parquet files and Schemas"
docs_dict['hostaxis'] = "Functions for automating/using the hostaxis API"
docs_dict['pdiddy'] = "Functions for automating/using the pdiddy API"
docs_dict['inventory'] = "Functions for getting information from Global Inventory"
docs_dict['util'] = "General Utility functions or unclassified functions"
docs_dict['cloud'] = "Functions related to cloud providers and systems"

def docs(funcname=None, node_dict=None):
    if node_dict is None:
        broot = True
        node_dict = docs_dict
    else:
        broot = False
    
    if broot: 
        print("PyResearch is a module containing curated functions provided by the research team")
        print("")
    
        if funcname in node_dict:
            print("Submodule: %s - %s" % (funcname, node_dict[funcname]))
            print("")
        else:
            print("The submodules are:")
            print("-------------------")
            print("")
            for d in node_dict:
                print("%s - %s" % (d, node_dict[d]))
    else:
        if funcname in node_dict:
            print("Function: %s - %s" % (funcname, node_dict[funcname]['shortdesc']))
            print("")
            print(node_dict[funcname]['desc'])
            print("")
            print("Arguments:\n--------\n%s" % node_dict[funcname]['fargs'])
            print("")
            print("Return: %s" % node_dict[funcname]['fret'])
            print("")
        else:
            print("The submodules are:")
            print("-------------------")
            print("")
            for f in node_dict:
                print("%s - %s" % (f, node_dict[f]['shortdesc']))
