# Parquet Functions
from collections import OrderedDict
from pyresearch.util.sharedfunctions import *
import pyresearch

docs_dict = OrderedDict()


#   docs_dict[''] = {'fargs':'',
#               'fret': '',
#               'shortdesc': '',
#               'desc': ''
#               }

docs_dict['fixBeakerBool'] = {'fargs':'input_df: The Input Dataframe to fix the boolean fields on ',
          'fret': 'output_df: The input_df variable with the boolean columns converted to Objects for display',
           'shortdesc': 'Convert boolean fields in a Dataframe to object for display in BeakerX',
           'desc': 'fixBeakerBool(input_df)\nBeakerX has a bug when it tries to display a Boolean column it errors out, this function fixes that'
            }
docs_dict['explode_array'] = {'fargs':'in_df: The dataframe containing the array column to be exploded\nexplode_col: The column of the input dataframe containing the array to be exploded',
          'fret': 'out_df: The dataframe created from the exploded array column',
          'shortdesc': 'Explodes a column of a dataframe that is in array format and create a new output dataframe from the exploded column',
          'desc': 'Columns in array format are difficult to read or work with. Using this function in conjuction with the join_dfs function creates a new dataframe preserving all data and data associations of the original dataframe, but in a much more useable and readable format.\n\nexample usage: hostaxis hosts resources airasia.com -c -1 -t 2019/01/01\ndf1 = prev_hostaxis_df\ndf2 = pyresearch.util.explode_array(df1, "resources")\ndf2 = pyresearch.util.join_dfs(df1, df2)\ndf2'
          }
docs_dict['join_dfs'] = {'fargs':'in_df1: The dataframe used as input for the explode_array function\nin_df2: The dataframe output from the explode_array function',
          'fret': "out_df: The dataframe created from joining the two input dataframes",
          'shortdesc': 'Joins 2 dataframes, one that orignally contained an array column, and one that was created from exploding said array column',
          'desc': 'Columns in array format are difficult to read or work with. Using this function in conjuction with the explode_array function creates a new dataframe preserving all data and data associations of the original dataframe, but in a much more useable and readable format.\n\nexample usage: hostaxis hosts resources airasia.com -c -1 -t 2019/01/01\ndf1 = prev_hostaxis_df\ndf2 = pyresearch.util.explode_array(df1, "resources")\ndf2 = pyresearch.util.join_dfs(df1, df2)\ndf2'
         }

docs_dict['isCIDR'] = {'fargs': 'The String representation of the CIDR to check', 
          'fret': 'True or False depending on if the string matches the CIDR regex', 
          'shortdesc': 'Take a string in and run a regex on it to determine if the string matches a CIDR block notation',
          'desc': 'Run a regex on a string, and if it matches a CIDR block, return True, else return False'
         }


def docs(funcname=None):
    pyresearch.docs(funcname, docs_dict)


