#
import pandas as pd
import pyresearch
import re
import json
import requests

def fixBeakerBool(input_df):
    # PUBLIC
    output_df = input_df
    for x in output_df.columns:
        if output_df.dtypes[x] == 'bool':
            output_df[x] = output_df[x].astype(str)
    return output_df

def explode_array(in_df, explode_col):
    # PUBLIC
    out_df = pd.concat({k: pd.DataFrame(array) for k, array in in_df.pop(explode_col).items()})
    return out_df

def join_dfs(in_df1, in_df2):
    # PUBLIC
    out_df = in_df2.reset_index(level=1, drop=True).join(in_df1, lsuffix='0').reset_index(drop=True)
    out_df = pyresearch.util.fixBeakerBool(out_df)
    return out_df


def isCIDR(cidr):
    # PUBLIC
    if re.match(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$', cidr):
        return True
    else:
        return False

def isRoutableCIDR(cidr):
    # PUBLIC 
    if re.match(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$', cidr):
        if re.match(r'^(10|192\.168)\.', cidr):
            return False
        else:
            return True
    else:
        return False

def classifier_fix_reviews(minhashID, reviewedType, reviewCode, dryrun=False):
 
    if reviewedType not in ['phish']:
        print("reviewedType of {0} not in list of allowed reviewTypes".format(reviewedType))
        return None
    if reviewCode not in ['D', 'C']:
        print("reviewCode of {0} not in list of allowed reviewCodes".format(reviewCode))
    
    endpointurl = "http://cm01.riskiq:8080/crawlmgr/service/classifier/fix-reviews?minhashID={0}&reviewedType={1}&reviewCode={2}".format(minhashID, reviewedType, reviewCode)
    if dryrun == True:
        print("Dryrun - not actually executing\nURL:{0}".format(endpointurl))
    else:
        res = requests.get(cleanerurl)
        print("Result: {}".format(res.status_code))
